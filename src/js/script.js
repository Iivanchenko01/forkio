const burger = document.querySelector('.header__burger');
const menu = document.querySelector('.header__nav');

function handlerBurger() {
  burger.classList.toggle('active');
  menu.classList.toggle('active');
}

burger.addEventListener('click', handlerBurger);


document.addEventListener("DOMContentLoaded", function() {
  const links = document.querySelectorAll(".nav__menu--item a");
  console.log(links);
  // Добавляем обработчик клика на первую ссылку
  links[0].addEventListener("click", function(e) {
    e.preventDefault();
    
    links.forEach(function(link) {
      link.classList.remove("active-link");
    });
    

    this.classList.add("active-link");
  });
});

